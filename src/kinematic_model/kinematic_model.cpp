#include "kinematic_model.h"

RobotModel::RobotModel(const double &l1In, const double &l2In):
  l1  (l1In),
  l2  (l2In)
{};

void RobotModel::FwdKin(Eigen::Vector2d &xOut, Eigen::Matrix2d &JOut, const Eigen::Vector2d & qIn){
  // Implementation of the forward and differential kinematics
  double c1 = cos(qIn(0));
  double c12 = cos(qIn(0)+qIn(1));
  double s1 = sin(qIn(0));
  double s12 = sin(qIn(0)+qIn(1));

  xOut << l1*c1+l2*c12, l1*s1+l2*s12; //position
  JOut << -l1*s1-l2*s12, -l2*s12, l1*c1+l2*c12, l2*c12; //jacobian matrix
}