#include "control.h"
#include <iostream>
Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{}

Eigen::Vector2d Controller::Dqd ( 
          const Eigen::Vector2d & q,
          const Eigen::Vector2d & xd,
          const Eigen::Vector2d & Dxd_ff
                      )
{  
  // Compute joint velocities able to track the desired cartesian position
  
  model.FwdKin(X,J,q);
  dX_desired = (xd-X)*kp + Dxd_ff;
  Eigen::Vector2d Dqd = J.inverse()*dX_desired;

  return Dqd; // returns joint velocities
}

