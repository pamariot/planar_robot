#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn)
{
  // Initialisation of the object polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;

  a[0] = pi;
  a[1] = a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = -15*(pf-pi);
  a[5] = 6*(pf-pi);
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn)
{
  // Update polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;

  a[0] = pi;
  a[1] = a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = -15*(pf-pi);
  a[5] = 6*(pf-pi);
};

const double  Polynomial::p     (const double &t)
{
  // Compute position
  double p = a[0] + a[3]*pow(t/Dt,3) + a[4]*pow(t/Dt,4) + a[5]*pow(t/Dt,5);
  return p;
};

const double  Polynomial::dp    (const double &t)
{
  // Compute velocity
  double dp = 3*a[3]*pow(t,2)/pow(Dt,3) + 4*a[4]*pow(t,3)/pow(Dt,4) + 5*a[5]*pow(t,4)/pow(Dt,5);
  return dp;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn)
{
  // Initialisation of object and polynomials
  auto X_i = xi;
  auto X_f = xf;
  auto Dt = DtIn;

  polx = Polynomial(X_i(0), X_f(0), Dt);
  poly = Polynomial(X_i(1), X_f(1), Dt);
}

Eigen::Vector2d Point2Point::X(const double & time)
{
  // Compute cartesian position
  Eigen::Vector2d X;
  X(0) = polx.p(time);
  X(1) = poly.p(time);
  return X;
}

Eigen::Vector2d Point2Point::dX(const double & time)
{
  // Compute cartesian velocity
  Eigen::Vector2d dX;
  dX(0) = polx.dp(time);
  dX(1) = poly.dp(time);
  return dX;
}