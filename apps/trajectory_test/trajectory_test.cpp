#include "trajectory_generation.h"
#include <iostream>
using namespace std;

int main(){
  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities*
  cout << "Trajectory test \n";
  Eigen::Vector2d Xi(0,0);
  Eigen::Vector2d Xf(1,1);
  double Dt = 2;

  auto trajectory = Point2Point(Xi, Xf, Dt);

  for(double t=0; t<Dt; t+=0.2)
  {
    auto pos = trajectory.X(t); 
    auto vit = trajectory.dX(t);
    Eigen::Vector2d vit_num(0,0);
    vit_num = (trajectory.X(t+1e-6)-pos)/1e-6;
    cout << "x = " << pos(0) << "\ty = " << pos(1) << "\tdx = " << vit(0) << "\tdy = " << vit(1) << "\tdxn = " << vit_num(0) << "\tdyn = " << vit_num(1) << "\t at t = " << t <<"\n";
  }
}