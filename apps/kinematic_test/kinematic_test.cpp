#include "kinematic_model.h"
#include <iostream>
using namespace std;

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  // For a small variation of q, compute the variation on X and check dx = J . dq  
  double l1 = 1;
  double l2 = 1;
  Eigen::Vector2d pos;
  Eigen::Matrix2d J;
  Eigen::Vector2d q(M_PI/3.0,M_PI_4);
  RobotModel MyModel = RobotModel(l1, l2);
  MyModel.FwdKin(pos, J, q);
  cout << "pos =\n" << pos << "\nJ =\n" << J << "\nfor q =\n" << q <<" \n";


  // analytic and numeric comparison
  Eigen::Vector2d q1(M_PI_2, M_PI_4);
  Eigen::Vector2d dq(0.01, 0.01);
  Eigen::Vector2d q2 = q1 + dq;

  Eigen::Vector2d x1 = J*q1;
  Eigen::Vector2d x2 = J*q2;
  Eigen::Vector2d dxn = x2-x1;
  Eigen::Vector2d dxa = J*dq;

  cout << "Comparing, dxa =\n" << dxa << "\nwhile dxn =\n" << dxn << "\n";  //seems good
}